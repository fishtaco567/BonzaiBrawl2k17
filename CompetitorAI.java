import java.util.List;

import Castles.api.*;
import bonzai.*;

/**
 * Simple example AI, which moves the first soldier in the AI's list of soldiers
 * towards the enemy base.
 * 
 * @author Joshua
 */
@Agent(name ="Test")
public class CompetitorAI extends AI {
	
	/* This AI does not use a default constructor! */
	
	@Override
	public Action action(Turn turn) {
		Team myTeam = turn.getMyTeam();
		
		return null;
	}
}
